export class User {
    id: number;
    name: string;
    password: string;
    email: string;
    firstName: string;
    lastName: string;
    access_token: string;
    refresh_token: string;
}