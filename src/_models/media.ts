export class Media {
    public id: number;
    public name: string;
    public user_id: number;
    public description: string;
    public path: string;
    public mime_type: string;
    public type: string;
    public server: string;
}
