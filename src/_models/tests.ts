export class Test {
    public id: number;
    public name: string;
    public slug: string;
    public image: string;
    public category: any;
}
