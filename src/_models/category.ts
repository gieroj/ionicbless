export class Category {
    public id: number;
    public name: string;
    public description: string;
    public slug: string;
    public image: string;
    public parent_id: number;
    public country_id: number;
}
