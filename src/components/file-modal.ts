import { Component, OnInit } from '@angular/core';
import { ModalController, ViewController } from 'ionic-angular';
import { Media } from '../_models/media';

import { MediaService } from '../_services/media.service';

@Component({
    templateUrl: 'file-modal.html'
})
export class FileModal {

    media:Media[] = [];
    chosen:number = 0;
    constructor(public viewCtrl: ViewController, private mediaService: MediaService,) {
        
    }

    ngOnInit(): void {
        this.getMedia();
    }

    async getMedia() {
        try {
            var response = await this.mediaService.getMyMedia();
            this.media = response['data'];
            console.log(response);
        }
        catch (e) {
            console.error(e);
        }
    }

    save() {
        this.viewCtrl.dismiss(this.chosen);
    }

    choseMedia(item: Media){
        this.chosen=item.id;
    }

    fullpath(item: Media){
        return item.server + item.path + item.name;
    }

    isChosen(id:number){
        if(id == this.chosen){
            return "2px Solid blue"; 
        }else{
            return;
        }
    }

    fileUpload(event:any) {
        let fileList: FileList = event.target.files;
        if(fileList.length > 0) {
            let file: File = fileList[0];
            console.log(file);
            
            this.saveMedia(file,"img");
            console.log("momo");
        }
    }

    async saveMedia(formData:File, type:string) {
        try {
            var new_media:Media;
            new_media = await this.mediaService.create(formData,type);
            this.media.unshift(new_media);
        }
        catch (e) {
            console.error(e);
        }
    }
    
    delete(){
        console.log(this.chosen);
        this.deleteMedia();
    }

    async deleteMedia() {
        try {
            let response:any;
            response = await this.mediaService.delete(this.chosen);
            console.log(response);
            if(response.meta.deleted){
            let index = this.findId(this.media,this.chosen);
            this.media.splice(index, 1);
            }
            // this.media.unshift(new_media);
        }
        catch (e) {
            console.error(e);
        }
    }

    findId(array:any[],search:number):number{
        for (var i in array) // for acts as a foreach  
        {  
            if(array[i].id == search){
                return Number(i);
            }
        } 
    }
}