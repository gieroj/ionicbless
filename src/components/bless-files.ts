import { Directive, HostListener, Output, EventEmitter } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { ModalController } from 'ionic-angular';

import { FileModal } from './file-modal';

@Directive({
  selector: '[blessfiles]'
})
export class BlessFiles {
  @Output() notify: EventEmitter<number> = new EventEmitter<number>();

  constructor( public modalCtrl: ModalController) { }
  
  media_id:number=0;

  @HostListener('click', ['$event'])onClick() {
    let profileModal = this.modalCtrl.create(FileModal , { media: this.media_id});
    profileModal.onDidDismiss(data => {
      this.media_id=data;
      this.notify.emit(data);
    });
   profileModal.present();
  }

  


}

