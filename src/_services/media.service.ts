import { Injectable } from '@angular/core';
import { Headers, Http, Response, RequestOptions } from '@angular/http';
import { MenuController } from 'ionic-angular';
import { CookieService } from 'ngx-cookie-service';
// import { Observable } from 'rxjs/Observable';

import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/Rx';

import { Media } from '../_models/media';
import { CONFIG } from '../_shared/env.config';

@Injectable()
export class MediaService {
    private myBearer:string;
    private headers = new Headers();
    private url =CONFIG.BASE_URI+"media";

    constructor(private cookieService: CookieService, private http: Http, public menuCtrl: MenuController) {
        this.myBearer = this.cookieService.get('access_token');
        if(!this.myBearer){
            this.myBearer=CONFIG.BEARER_PUBLIC;
        }
        this.headers = new Headers({
            'Content-type': 'application/x-www-form-urlencoded; charset=utf-8', 
            'Authorization': 'Bearer ' + this.myBearer,
        });
    }

    getMedia(): Promise<Media[]> {
        var options = new RequestOptions({ headers: this.headers });
        return this.http.get(this.url, options)
            .toPromise()
            .then((res: Response) => {
                return res.json();
            })
            .catch(this.handleError);
    }

    getMyMedia(): Promise<Media[]> {
        var options = new RequestOptions({ headers: this.headers });
        return this.http.get(this.url, options)
            .toPromise()
            .then((res: Response) => {
                // console.log(res);
                return res.json();
            })
            .catch(this.handleError);
    }

    create(file:File, type:string): Promise<Media> {
        var headers = new Headers({
            'Authorization': 'Bearer ' + this.myBearer
        });
        
        let formData:FormData = new FormData();
        formData.set('file', file);
        
        
        var data : {
            type: string;
            file: File;
            } = {
            type: "img",
            file: file
            };
        formData.append("type", type);
        console.log(headers,formData);
        // var options = new RequestOptions({ headers: headers });
        return this.http.post(this.url, formData, { headers: headers })
            .toPromise()
            .then((res: Response) => {
                return res.json();
            })
            .catch(this.handleError);
    }


    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error); // for demo purposes only
        return Promise.reject(error.message || error);
    }


   

    delete(id: number): Promise<void> {
        return this.http.delete(this.url+"/"+id, { headers: this.headers })
            .toPromise()
            .then((res: Response) => {
                return res.json();
            })
            .catch(this.handleError);
    }

}
