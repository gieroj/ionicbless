import { Injectable } from '@angular/core';
import { Headers, Http, Response, RequestOptions } from '@angular/http';
import { NavController, MenuController } from 'ionic-angular';
import { CookieService } from 'ngx-cookie-service';
// import { Observable } from 'rxjs/Observable';

import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/Rx';

import { User } from '../_models/user';

import { CONFIG } from '../_shared/env.config';

@Injectable()
export class UserService{
    private myBearer:string;

    constructor(private cookieService: CookieService, private http: Http, public menuCtrl: MenuController) {
        this.myBearer = this.cookieService.get('access_token');
    }

    obtainAccessToken(user: User) {
        //To jest kod pozwalajacy na dostanie tokena przez password WIN WIN WIN
        let params = new URLSearchParams();
        params.append('grant_type', 'password');
        params.append('client_id', CONFIG.CLIENT_ID );
        params.append('username', user.email);
        params.append('password', user.password);
        params.append('client_secret', CONFIG.CLIENT_SECRET);
        let headers = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' });
        let options = new RequestOptions({ headers: headers });

        this.http.post(CONFIG.OAUTH_URI+'token', params.toString(), options)
            .map(res => res.json())
            .subscribe(
            data => this.saveToken(data),
            err => {
                console.log(err.json());
            });
    }

    saveToken(token) {
         console.log(token);
        var expireDate = new Date().getTime() + (1000 * token['expires_in']);
        this.cookieService.set('access_token', token['access_token'], expireDate);
        this.myBearer = token.access_token;
    }

    logout() {
        this.cookieService.delete('access_token');
        this.myBearer = "";
    }

    createUser(user: User) {
        let params = new URLSearchParams();
        params.append('name', user.email);
        params.append('email', user.email);
        params.append('password', user.password);
        let headers = new Headers({
            'Content-Type': 'application/x-www-form-urlencoded',
            'Authorization': 'Bearer '+CONFIG.BEARER_PRIVATE,
        });
        let options = new RequestOptions({ headers: headers });
        return this.http.post(CONFIG.BASE_URI+'register', params.toString(), options)
            .toPromise()
            .then((res: Response) => {
                console.log(res);
                if (res['_body'].indexOf("The email has already been taken.")) {
                    console.log("taken");
                    this.loginUser(user);
                }
                return res;
            })
            .catch(this.handleError);
    }

    loginUser(user: User): Promise<void> {
        //To jest kod pozwalajacy na dostanie tokena przez password WIN WIN WIN
        let params = new URLSearchParams();
        params.append('grant_type', 'password');
        params.append('client_id', CONFIG.CLIENT_ID );
        params.append('username', user.email);
        params.append('password', user.password);
        params.append('client_secret', CONFIG.CLIENT_SECRET);
        let headers = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' });
        let options = new RequestOptions({ headers: headers });

        return this.http.post(CONFIG.OAUTH_URI+'token', params.toString(), options)
            .toPromise()
            .then((res: Response) => {
                let data = res.json();
                this.saveToken(data);
                res => res.json();
            })
            .catch(this.handleError);
    }

    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error); // for demo purposes only
        return Promise.reject(error.message || error);
    }
    
    public islogged(){
        // console.log('myBearer',this.myBearer);
        if(this.myBearer){
            return true;
        }else{
            return false;
        }
    }

    openMenu(evt, navCtrl: NavController) {
        console.log(this.myBearer);
        if (evt === "left_menu") {
            this.menuCtrl.enable(true, 'left_menu');
            this.menuCtrl.enable(false, 'admin_menu');
        } else {
            if (this.myBearer) {
                this.menuCtrl.enable(true, 'admin_menu');
                this.menuCtrl.enable(false, 'left_menu');
            }else{
                this.menuCtrl.enable(false, 'left_menu');
                this.menuCtrl.enable(false, 'admin_menu');
                navCtrl.push('RegisterPage');
            }
        }
        this.menuCtrl.toggle();
    }
}
