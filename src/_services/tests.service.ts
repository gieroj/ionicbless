import { Injectable } from '@angular/core';
import { Headers, Http, Response, RequestOptions } from '@angular/http';
import { MenuController } from 'ionic-angular';
import { CookieService } from 'ngx-cookie-service';
// import { Observable } from 'rxjs/Observable';

import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/Rx';

import { Test } from '../_models/tests';
import { Button } from '../_shared/buttons';
import { CONFIG } from '../_shared/env.config';

@Injectable()
export class TestsService {
    private myBearer:string;
    private heroesUrl = 'http://blesstest2.com/api/test';

    private headers = new Headers({
        'Content-type': 'application/x-www-form-urlencoded; charset=utf-8', 
        'Authorization': 'Bearer ' + this.myBearer,
    });

    constructor(private cookieService: CookieService, private http: Http, public menuCtrl: MenuController) {
        this.myBearer = this.cookieService.get('access_token');
        if(!this.myBearer){
            this.myBearer=CONFIG.BEARER_PUBLIC;
        }
        this.headers = new Headers({
            'Content-type': 'application/x-www-form-urlencoded; charset=utf-8', 
            'Authorization': 'Bearer ' + this.myBearer,
        });
    }

    getTests(): Promise<Test[]> {
        var options = new RequestOptions({ headers: this.headers });
        return this.http.get(CONFIG.BASE_URI+"test", options)
            .toPromise()
            .then((res: Response) => {
                return res.json();
            })
            .catch(this.handleError);
    }

    getTest(id: number): Promise<Test> {
        console.log(this.heroesUrl + '/' + id);
        const url = `${CONFIG.BASE_URI}test/${id}`;
        var headers = new Headers({ 'Authorization': 'Bearer ' + this.myBearer });
        var options = new RequestOptions({ headers: headers });
        return this.http.get(url, options)
            .toPromise()
            .then((res: Response) => {
                console.log(res.json());
                return res.json();
            })
            .catch(this.handleError);
    }

    getMyTests(): Promise<Test[]> {
        console.log(this.headers);
        var options = new RequestOptions({ headers: this.headers });
        return this.http.get(CONFIG.BASE_URI+"mytest", options)
            .toPromise()
            .then((res: Response) => {
                return res.json();
            })
            .catch(this.handleError);
    }

    createTest(test:Test): Promise<Test[]> {
        console.log(test);
        // var options = new RequestOptions({ headers: this.headers });
        return this.http.post(CONFIG.BASE_URI+"mytest", JSON.stringify({ test }), { headers: this.headers })
            .toPromise()
            .then((res: Response) => {
                return res.json();
            })
            .catch(this.handleError);
    }

    saveQuestion(buttons: any) {
        var options = new RequestOptions({ headers: this.headers });
        this.http.post(CONFIG.BASE_URI+"test", options)
            .toPromise()
            .then((res: Response) => {
                console.log(res.json());
                return res.json();
            })
            .catch(this.handleError);
    }

    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error); // for demo purposes only
        return Promise.reject(error.message || error);
    }


    update(hero: Button): Promise<Button> {
        return this.http
            .put(CONFIG.BASE_URI+"test/"+hero.id, JSON.stringify(hero), { headers: this.headers })
            .toPromise()
            .then(() => hero)
            .catch(this.handleError);
    }

    create(name: string): Promise<Button> {
        return this.http
            .post(CONFIG.BASE_URI+"test", JSON.stringify({ name: name }), { headers: this.headers })
            .toPromise()
            .then(res => res.json().data)
            .catch(this.handleError);
    }

    delete(id: number): Promise<void> {
        // const url = `${this.heroesUrl}/${id}`;
        return this.http.delete(CONFIG.BASE_URI+"test/"+id, { headers: this.headers })
            .toPromise()
            .then(() => null)
            .catch(this.handleError);
    }

}
