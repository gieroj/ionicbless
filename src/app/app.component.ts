import { Component, ViewChild } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';


import { UserService } from '../_services/user.service';
import { IonicPage, NavController, MenuController } from 'ionic-angular';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild('content') navCtrl: NavController;
  rootPage: string = 'TestsPage';

  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen, public menuCtrl: MenuController, private userService: UserService) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
      this.menuCtrl.enable(true, 'left_menu');
      this.menuCtrl.enable(true, 'admin_menu');  
    });
  }
  

  closeMenu() {
    this.menuCtrl.close();
  }
  
  logout() {
    this.userService.logout();
    this.navCtrl.push('TestsPage');
  }
  
  gotoPage(page:string) {
    this.navCtrl.push(page);
    this.menuCtrl.toggle();
  }

}

