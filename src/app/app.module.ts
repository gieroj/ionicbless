import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule, IonicPageModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { HttpModule } from '@angular/http';
import { Base64 } from '@ionic-native/base64';

import { ResizableModule } from 'angular-resizable-element';
import { ColorPickerModule } from 'ngx-color-picker';
import { CookieService } from 'ngx-cookie-service';

import { MyApp } from './app.component';
import { TestsService } from '../_services/tests.service'
import { MediaService } from '../_services/media.service'
import { CategoryService } from '../_services/category.service'
import { UserService } from '../_services/user.service'

import { FileModal } from '../components/file-modal';

@NgModule({
  declarations: [
    MyApp,
    FileModal
  ],
  imports: [
    BrowserModule,
    ResizableModule,
    HttpModule,
    ColorPickerModule,
    IonicModule.forRoot(MyApp),
    IonicPageModule.forChild(MyApp),
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    FileModal
  ],
  providers: [
    TestsService,
    CategoryService,
    UserService,
    StatusBar,
    SplashScreen,
    CookieService,
    MediaService,
    Base64,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ],
  exports: [
    ColorPickerModule
  ],
})
export class AppModule {}
