import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, MenuController } from 'ionic-angular';

import { CategoryService } from '../../_services/category.service';
import { User } from '../../_models/user';

import { Category } from '../../_models/category';
import { UserService } from '../../_services/user.service';

@IonicPage({})
@Component({
    selector: 'page-category',
    templateUrl: 'category.html'
})
export class CategoryPage implements OnInit {

    category: Category[] = [];
    pushPage: any;
    isDesc: boolean = true;
    column: string ='name';

    constructor(public navCtrl: NavController, private categoryService: CategoryService,  public menuCtrl: MenuController, private userService: UserService) {
        
    }

    ngOnInit(): void {
        this.getCategories();
    }

    async getCategories(){
        try {
            this.category= await this.categoryService.getCategories();
            console.log(this.category);
        }
        catch (e) {
            console.error(e);
        }
    }

    login() {
         this.navCtrl.push('RegisterPage');
    }


    sortBy(property:string){
        this.isDesc = !this.isDesc;
        this.column = property;
        let direction = this.isDesc ? 1 : -1;
        this.category.sort(function(a,b){
            if(a[property] < b[property]){
                return -1 * direction;
            }
            else if( a[property] > b[property]){
                return 1 * direction;
            }
            else{
                return 0;
            }
        });
    }

}


