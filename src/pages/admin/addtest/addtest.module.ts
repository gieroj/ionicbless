import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AddtestPage } from './addtest';

import { BlessFiles } from '../../../components/bless-files';


@NgModule({
  declarations: [
    AddtestPage,
    BlessFiles
  ],
  imports: [
    
    IonicPageModule.forChild(AddtestPage),
  ],
})
export class AddtestPageModule {}
