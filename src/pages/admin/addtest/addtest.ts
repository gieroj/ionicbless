import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { UserService } from '../../../_services/user.service';
import { TestsService } from '../../../_services/tests.service';
import { User } from '../../../_models/user';

import { Test } from '../../../_models/tests';

import { BlessFiles } from '../../../components/bless-files';

@IonicPage({})
@Component({
    selector: 'page-addtest',
    templateUrl: 'addtest.html'
})
export class AddtestPage implements OnInit {
    test?: Test;
    id: number;
    uploadfile:any;
    media_id:number=0;
    
    constructor(public navCtrl: NavController, private testsService: TestsService,private userService: UserService, 
        public navParams: NavParams ) {
        this.id = navParams.data;
        this.test= new Test;
    }

    ionViewCanEnter(): boolean {
        if (this.userService.islogged()) {
            return true;
        } else {
            this.navCtrl.push('RegisterPage');
        }
    }

    ngOnInit(): void {
        
    }
    async createtest(){
        console.log(this.uploadfile);
        try {
            const result = await this.testsService.createTest(this.test);
            console.log(result);
        }
        catch (e) {
            console.error(e);
        }
    }

    fileChange(event) {
        console.log(this.media_id);
    }

    login() {
        console.log(this.test);
        // this.navCtrl.push(RegisterPage);
    }

    onNotify(message:number):void {
        console.log(message);
    }


}


