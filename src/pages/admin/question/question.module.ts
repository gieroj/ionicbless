import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { QuestionPage } from './question';

import { ColorPickerModule } from 'ngx-color-picker';
import { ResizableModule } from 'angular-resizable-element';

@NgModule({
  declarations: [
    QuestionPage,
  ],
  imports: [
    ResizableModule,
    ColorPickerModule,
    IonicPageModule.forChild(QuestionPage),
  ],
})
export class QuestionPageModule {}
