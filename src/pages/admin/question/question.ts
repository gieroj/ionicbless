import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { ResizeEvent } from 'angular-resizable-element';
import { ColorPickerService } from 'ngx-color-picker';

import { Button } from '../../../_shared/buttons';
import { DragDropShim } from '../../../_shared/dragdropshim';

import { UserService } from '../../../_services/user.service';
import { TestsService } from '../../../_services/tests.service';
import { User } from '../../../_models/user';

@IonicPage({})
@Component({
    selector: 'page-home',
    templateUrl: 'question.html'
})
export class QuestionPage implements OnInit {

    startDrag: any;
    stopDrag: any;
    onDragButton: any;
    onStartDragEvent: any;
    fakebutton: Button[] = [];
    resizeflag: boolean = true;
    maxPoints: number = 0;
    minPoints: number = 0;
    dragPanel: boolean = true;
    dropPanel: boolean = false;
    trashPanel: boolean = false;

    button = [new Button(0, '<p>Button</p>', 'button', '#e6e6e6', '#333', ['6', '12', '6', '12']),
    new Button(1, '<p>Check Box</p>', 'checkbox', '#ff0000', '#333', ['6', '12', '6', '12']),
    new Button(2, '<p>Place holderos</p>', 'div', '#e6e6e6'),
    new Button(3, '<p>Radio Button</p>', 'radio', '#ff0000', '#333', ['6', '12', '6', '12']),
    new Button(4, '<p>Submit</p>', 'submit', '#e6e6e6', '#333', ['6', '12', '6', '12']),
    new Button(5, '<p>Gemini 1</p>', 'gemini', '#e6e6e6', '#333', ['6', '12', '6', '12']),
    new Button(6, '<p>Gemini 2</p>', 'gemini', '#e6e6e6', '#333', ['6', '12', '6', '12']),
    ];

    allowCopy: boolean = true;

    menu: any;

    constructor(public navCtrl: NavController, public navParams: NavParams, private testsService: TestsService, private userService: UserService) {
        this.menu = new menu();
    }

    ionViewCanEnter(): boolean {
        if (this.userService.islogged()) {
            return true;
        } else {
            this.navCtrl.push('RegisterPage');
        }
    }

    ngOnInit(): void {

        new DragDropShim({ holdToDrag: 300, enableEnterLeave: true }, document);

        this.button[5].connector = 6;
        this.button[6].connector = 5;
    }

    onResizeEnd(event: ResizeEvent, button: any) {
        // console.log('onResizeEnd event ', event,"button ", button);
        this.button[button.id].width = event.rectangle.width;
        this.button[button.id].height = event.rectangle.height;
        if ((event.edges.left > 0) || (event.edges.left < 0)) {
            this.button[button.id].dropX = this.button[button.id].dropX + Number(event.edges.left);
        }
        if ((event.edges.top > 0) || (event.edges.top < 0)) {
            this.button[button.id].dropY = this.button[button.id].dropY + Number(event.edges.top);
        }
        this.resizeflag = false;
    }

    onResize(event: ResizeEvent, button: any) {
        // console.log('Element Start resized', event, button);
        button.height = event.rectangle.height;
        button.width = event.rectangle.width;
        // event.rectangle.top = button.dropY;
        // console.log('event ', event);
    }


    dragStart($event: any) {
        this.startDrag = $event;
        //  console.log('dragStart ', $event);
        // console.log($event);
        //        document.getElementById('button'+(this.button.length - 1)).innerHTML = $event.dragData.msg; 
    }

    onDragStart($event: any, button: any) {
        this.onDragButton = button;
        this.onStartDragEvent = $event;
        // console.log('onDragStart ', this.onStartDragEvent, button);
        //       let why: any = $event.dataTransfer;
        // $event.dataTransfer.setDragImage(this, 0, 0);
        setTimeout(() => {    //<<<---    using ()=> syntax
            this.dragPanel = false;
            this.dropPanel = true;
        }, 100);
        //   why.setData('Text','oki');
        $event.stopPropagation();
        // $event.preventDefault();
        //   why.dropEffect = "all";

    }
    onDragEnter($event: any) {
        $event.preventDefault();
    }
    onDragOver($event: any) {
        this.stopDrag = $event;
        // console.log('onDragOver ', $event);
        $event.stopPropagation();
        $event.preventDefault();

    }

    onDragLeave($event: any) {
        //    console.log("onDragLeave",$event );
        // console.log(this.findParentClass( $event.target,"droppable"));
        if (!this.findParentClass($event.target, "droppable")) {
            // console.log("this.stopDrag = false",$event );
            this.stopDrag = false;
        }
    }

    onDrop($event: any) {
        //  if(this.stopDrag){
        //    console.log('onDrop ', $event, this.onDragButton,'this.stopDrag', this.stopDrag);
        // this.button.push(Object.assign({}, obj1);
        this.button.push(new Button(this.button.length, this.onDragButton.msg, this.onDragButton.typ, this.onDragButton.backgroundcolor,
            this.onDragButton.color, this.onDragButton.padding, this.onDragButton.width, this.onDragButton.height));//$event.mouseEvent, this.startDrag.mouseEvent));
        this.button[this.button.length - 1].position(this.stopDrag, this.onStartDragEvent);
        this.button[this.button.length - 1].borderradius = this.onDragButton.borderradius;
        if (this.onDragButton.connector > 0) {
            this.button[this.button.length - 1].connector = this.button.length;
            let sourceButton = this.button[this.onDragButton.connector];
            this.button.push(new Button(this.button.length, sourceButton.msg, sourceButton.typ, sourceButton.backgroundcolor,
                sourceButton.color, sourceButton.padding, sourceButton.width, sourceButton.height));//$event.mouseEvent, this.startDrag.mouseEvent));
            this.button[this.button.length - 1].position(this.stopDrag, this.onStartDragEvent);
            this.button[this.button.length - 1].dropY = this.button[this.button.length - 1].dropY + this.button[this.button.length - 1].height + 20;
            this.button[this.button.length - 1].connector = this.button.length - 2;
            this.button[this.button.length - 2].connectorX = (this.button[this.button.length - 1].dropX - this.button[this.button.length - 2].dropX) + this.button[this.button.length - 1].width / 2;
            this.button[this.button.length - 2].connectorY = (this.button[this.button.length - 1].dropY - this.button[this.button.length - 2].dropY) + this.button[this.button.length - 1].height / 2;
            console.log("conX", this.button[this.button.length - 1].connectorX);
            console.log("conY", this.button[this.button.length - 1].connectorY);
        }

        //   $event.preventDefault();
        // console.log('onDrop ', $event, this.onDragButton);
        //}else{
        //    console.log('onDrop but this.stopDrag == false');
        // }
    }

    onDrag($event: any) {
        //  console.log("onDrag ");
        // this.dragPanel= false;
        // this.dropPanel= true;
    }

    onDropMove($event: any) {
        if (this.stopDrag) {
            //   console.log('onDropMove ', $event, this.onDragButton,'this.stopDrag', this.stopDrag);
            // this.button.push(Object.assign({}, obj1);
            // this.button.push(new container(this.button.length, this.onDragButton.msg, this.onDragButton.typ, this.onDragButton.backgroundcolor, this.onDragButton.color, this.onDragButton.padding));//$event.mouseEvent, this.startDrag.mouseEvent));
            this.button[this.onDragButton.id].position(this.stopDrag, this.onStartDragEvent);
            // $event.preventDefault();
            // console.log('onDrop ', $event, this.onDragButton);
        }
    }

    dragStartCenter($event: any) {
        this.startDrag = $event;
        this.allowCopy = false;

        // console.log('dragStart ', $event);
    }

    showDragPanel($event: any) {
        if (this.dragPanel == true) {
            this.dragPanel = false;
        } else {
            this.dragPanel = true;
        }
    }

    showMenu(button_id: number) {
        if (this.resizeflag) {
            console.log("Show Menu ", button_id);
            this.menu.button = true;
            this.menu.id = button_id;
            this.menu.show = true;
        } else {
            this.resizeflag = true;
        }
    }

    showTab($event: any, id: string) {
        // console.log("showTab event",$event, "id",id );
        for (let i in this.menu.panel) {
            this.menu.panel[i] = false;
        }
        this.menu.panel[id] = true;
    }

    menumsg(newValue: any, id: number) {
        // console.log(newValue+" " +id);
        document.getElementById('button' + id).innerHTML = this.button[id].msg;
    }

    findParentClass(mouseEvent: any, argument: string): any {

        if (mouseEvent.classList.contains(argument)) {
            return mouseEvent;
        } else if (!(mouseEvent.parentElement.nodeName == "BODY")) {
            return this.findParentClass(mouseEvent.parentElement, argument);
        } else {
            return false;
        }
    }

    sumPoints() {
        this.maxPoints = 0;
        this.minPoints = 0;
        for (let i in this.button) {
            if (Number(i) > 5) {
                if (this.button[i].points > 0) {
                    this.maxPoints = Number(this.maxPoints) + Number(this.button[i].points);
                } else {
                    this.minPoints = Number(this.minPoints) + Number(this.button[i].points);
                }
            }
        }

    }

    savePanel($event: any) {
        console.log($event);
        this.testsService.saveQuestion(this.button);
    }

}



class menu {
    public button: true;
    public id: number;
    public show = false;
    public panel: boolean[] = [true, false, false];
    constructor() { }

}
