import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MytestsPage } from './mytests';

@NgModule({
  declarations: [
    MytestsPage,
  ],
  imports: [
    IonicPageModule.forChild(MytestsPage),
  ],
})
export class MyTestsPageModule {}
