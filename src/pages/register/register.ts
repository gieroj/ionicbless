import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { User } from "../../_models/user";

import { UserService } from '../../_services/user.service';

/**
 * Generated class for the RegisterPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage({})
@Component({
  selector: 'page-register',
  templateUrl: 'register.html',
})
export class RegisterPage {

  user = new User();
  public login = true;

  constructor(public navCtrl: NavController, public navParams: NavParams, private userService: UserService) {

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RegisterPage');
  }

  async register(user: User) {
    try {
      const result = await this.userService.createUser(user);
      //  const result = await this.afAuth.auth.createUserWithEmailAndPassword(user.email, user.password);
      console.log(result);
    }
    catch (e) {
      console.error(e);
    }
  }

  async loginuser(user: User) {
    try {
      const result = await this.userService.loginUser(user);
      //  const result = await this.afAuth.auth.createUserWithEmailAndPassword(user.email, user.password);
      console.log(result);
      this.navCtrl.push('QuestionPage');
    }
    catch (e) {
      console.error(e);
    }
  }

  togle() {
    console.log('togle');
    if (this.login) {
      this.login = false;
    } else {
      this.login = true;
    }
  }

}
