import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';


import { TestsService } from '../../_services/tests.service';

import { Test } from '../../_models/tests';

@IonicPage({})
@Component({
    selector: 'page-test',
    templateUrl: 'test.html'
})
export class TestPage implements OnInit {

    test: Test;
    id: number;

    constructor(public navCtrl: NavController, private testsService: TestsService, public navParams: NavParams) {
        this.id = navParams.get('id');
    }

    ngOnInit(): void {
        this.getTests();
    }

    async getTests(){
        try {
            this.test= await this.testsService.getTest(this.id);
            console.log(this.test);
        }
        catch (e) {
            console.error(e);
        }
    }

    login() {
        console.log(this.test);
        // this.navCtrl.push(RegisterPage);
    }


}


