import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, MenuController } from 'ionic-angular';

import { UserService } from '../../_services/user.service';
import { TestsService } from '../../_services/tests.service';
import { User } from '../../_models/user';


import { Test } from '../../_models/tests';

@IonicPage({})
@Component({
    selector: 'page-tests',
    templateUrl: 'tests.html'
})
export class TestsPage implements OnInit {

    tests: Test[] = [];
    isDesc: boolean = true;
    column: string = 'name';

    constructor(public navCtrl: NavController, private testsService: TestsService, private userService: UserService, public menuCtrl: MenuController) {

    }

    ngOnInit(): void {
        this.getTests();
    }


    async getTests() {
        try {
            this.tests = await this.testsService.getTests();
            console.log(this.tests);
        }
        catch (e) {
            console.error(e);
        }
    }

    addTest() {
        console.log(this.tests);
        // this.navCtrl.push(RegisterPage);
    }

    sortBy(property: string) {
        this.isDesc = !this.isDesc;
        this.column = property;
        let direction = this.isDesc ? 1 : -1;
        this.tests.sort(function (a, b) {
            if (a[property] < b[property]) {
                return -1 * direction;
            }
            else if (a[property] > b[property]) {
                return 1 * direction;
            }
            else {
                return 0;
            }
        });
    }

    gotoPage(test_id:number) {
        // console.log(test_id);
        this.navCtrl.push('TestPage',{id:test_id});
    }

}


