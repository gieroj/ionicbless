
export class Button {
    // private layerStartX: number = 0;
    // private layerStartY: number = 0;

    private layerStopX: number = 0;
    private layerStopY: number = 0;

    public dropX: number = 0;
    public dropY: number = 0;

    public width: number = 200;
    public height: number = 50;

    public display: string = "block";
    public id: number = 0;
    public msg: string = "<p>String</p>";
    public typ: string = "div";
    public backgroundcolor: string = "#ddd";
    public color: string = "#000";
    public padding = [6, 15, 6, 15];
    public borderwidth: number = 1;
    public bordercolor: string = '#adadad';
    public borderradius: number = 4;

    public points: number = 0;

    public connector: number = 0;
    public connectorX: number = 200;
    public connectorY: number = 200;

    constructor(id: number, msg?: string, typ?: string, backgroundcolor?: string, color?: string, padding?: any, width?: number, height?: number) {
        this.id = id;
        if (msg) { this.msg = msg; }
        if (typ) { this.typ = typ; }
        if (backgroundcolor) { this.backgroundcolor = backgroundcolor; }
        if (color) { this.color = color; }
        if (width) { this.width = width; }
        if (height) { this.height = height; }
        if (padding) { this.padding = padding; }
    }

    position(museStopEvent: any, museStartEvent: any) {
        //  console.log("museStopEvent",museStopEvent, museStartEvent);
        let stopTarget = this.findParentArgument(museStopEvent.target, "droppable");
        //   console.log("Position ", stopTarget);
        let parent = stopTarget.getBoundingClientRect();
        // console.log(parent);
        if (museStopEvent) {
            // console.log("parent", parent);
            this.layerStopX = museStopEvent.clientX;
            this.layerStopY = museStopEvent.clientY;
        }

        if (museStartEvent) {
            // this.layerStartX = museStartEvent.layerX;
            // this.layerStartY = museStartEvent.layerY;
            if (museStartEvent.target.offsetWidth) {
                this.width = museStartEvent.target.offsetWidth;
            }
            if (museStartEvent.target.offsetHeight) {
                this.height = museStartEvent.target.offsetHeight;
            }
        }

        if (museStopEvent && museStartEvent) {
            //  // let helpX = museStartEvent.clientX - museStartEvent.target.offsetLeft;
            //  // this.dropX = this.layerStopX - parent.left - helpX - 15;
            //  if(museStartEvent.target.offsetWidth){
            //      this.dropX =this.layerStopX - parent.left - museStartEvent.clientX;
            //  }else{
            //      this.dropX =this.layerStopX - parent.left + 10 - museStartEvent.clientX;
            //  }
            //  console.log("dropX",this.dropX,this.layerStopX, parent.left, museStopEvent, museStartEvent);
            //  // console.log("sssss", this.dropX, this.layerStopX, parent.left, help, museStartEvent.clientX, museStartEvent.target.offsetLeft);
            //  if (this.dropX < 0) {
            //      this.dropX = 0;
            //  }else if(parent.width<this.dropX+this.width){
            //      this.dropX=parent.width-this.width;
            //  }
            //  //  console.log("parent.width",parent.width,"this.dropY",this.dropX,"this.width",this.width);

            //  // console.log("dropek ", this.dropX, parent.width, museStartEvent.target.clientWidth, museStartEvent.target.offsetLeft);
            //  // if ((this.dropX + (museStartEvent.clientX - helpX) -30 ) > parent.width) {
            //  //     console.log("to big man");
            //  //     this.dropX = parent.width - museStartEvent.target.clientWidth - 2;
            //  // }
            //  // let helpY = museStartEvent.clientY - museStartEvent.target.offsetTop;
            //  // console.log("museStartEvent.clientY",museStartEvent.clientY,"museSartEvent.target.offsetTop",museStartEvent.target.offsetTop, "museStartEvent.target",museStartEvent.target);
            //  //this.dropY = this.layerStopY - parent.top - helpY - 15;

            //  if(museStartEvent.target.offsetHeight){
            //      this.dropY =this.layerStopY - parent.top - museStartEvent.offsetY;
            //  }else{
            //      this.dropY =this.layerStopY - parent.top - 20;
            //  }
            //  // console.log("dropY ", this.dropY,"layerStopY", this.layerStopY,"parent.top",parent.top,"helpY ", helpY );
            //  if (this.dropY < 0) {
            //      this.dropY = 0;
            //  }else if(parent.height<this.dropY+this.height){
            //      this.dropY=parent.height-this.height;
            //  }
            this.dropY = museStopEvent.clientY - parent.top - (this.height / 2);
            this.dropX = museStopEvent.clientX - parent.left - (this.width / 2);
            if (museStopEvent.offsetY) {
                this.dropY = this.layerStopY - parent.top - 21;
            }
            if (museStopEvent.offsetX) {
                this.dropX = this.layerStopX - parent.left + 2 - museStartEvent.offsetX;
            }
            // console.log("dropY",this.dropY, this.layerStopY, parent.top, museStartEvent.offsetY);
            if (this.dropX < 0) {
                this.dropX = 0;
            }
            if (this.dropY < 0) {
                this.dropY = 0;
            }
            // console.log("parent.offsetHeight",parent.height,"this.dropY",this.dropY,"this.height",this.height);

            // if (this.dropY + museStartEvent.target.clientHeight > stopTarget.clientHeight) {
            //     this.dropY = stopTarget.clientHeight - museStartEvent.target.clientHeight ;
            // }
            // if (this.dropX < 0) {
            //     this.dropX = 0;
            // }
            // this.dropY = this.layerStopY - this.layerStartY;
            // if (this.dropY + museStartEvent.target.clientHeight > stopTarget.clientHeight) {
            //     this.dropY = stopTarget.clientHeight - museStartEvent.target.clientHeight;
            // }
            // if (this.dropY < 0) {
            //     this.dropY = 0;
            // }
            //                        console.log(museStopEvent);
        }
    }

    findParentArgument(mouseEvent: any, argument: string): any {
        // console.log("mouseEvent",mouseEvent);
        if (mouseEvent.classList.contains(argument)) {
            return mouseEvent;
        } else if (!(mouseEvent.parentElement.nodeName == "BODY")) {
            return this.findParentArgument(mouseEvent.parentElement, argument);
        } else {
            return false;
        }
    }
}